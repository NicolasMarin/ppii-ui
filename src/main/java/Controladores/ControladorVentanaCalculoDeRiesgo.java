package Controladores;

import monitorDeRiesgos.MonitorDeRiesgos;
import ppii.ui.VentanaCalculoDeRiesgo;

public class ControladorVentanaCalculoDeRiesgo {
	
	VentanaCalculoDeRiesgo myView;
	MonitorDeRiesgos myModel;
	
	public ControladorVentanaCalculoDeRiesgo(VentanaCalculoDeRiesgo ventana) {
		myView = ventana;
		myModel = ventana.getMyModel();		
	}
	
	public void inicializar() {
		//...
	}
	
}
