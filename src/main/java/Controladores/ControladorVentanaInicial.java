package Controladores;

import java.util.ArrayList;
import java.util.List;

import monitorDeRiesgos.MonitorDeRiesgos;
import pojos.ZonaRiesgo;
import ppii.ui.VentanaCalculoDeRiesgo;
import ppii.ui.VentanaInicial;
import riesgoPackage.RiesgoInterface;

public class ControladorVentanaInicial {
	
	VentanaInicial myView;
	MonitorDeRiesgos myModel;
	
	public ControladorVentanaInicial(VentanaInicial ventana) {
		myView = ventana;
		myModel = ventana.getMyModel();
	}
	
	public void inicializar() throws InstantiationException, IllegalAccessException {	
		myView.getBtnCalcular().addActionListener(accion        -> calcular());
	}	
	
	private void calcular() {
		List <RiesgoInterface> riesgos = new ArrayList();
		int[] filas= myView.getTablaRiesgos().getSelectedRows();
		for (int fila: filas) {
			String riesgo = myView.getTablaRiesgos().getValueAt(fila, 0).toString();
			for(RiesgoInterface r : myModel.getRiesgos()) {
				if(r.getNombre().equals(riesgo)) {
					riesgos.add(r);
				}
			}
		}
		VentanaCalculoDeRiesgo vista =  new VentanaCalculoDeRiesgo(myModel);
		vista.setRiesgosSeleccionados(riesgos);
		vista.inicializar();
	}
			
}