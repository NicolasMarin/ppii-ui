package Controladores;

import monitorDeRiesgos.MonitorDeRiesgos;
import ppii.ui.VentanaInicial;

public class Main {

	public static void main(String[] args) throws Exception{
		
		MonitorDeRiesgos modelo = new MonitorDeRiesgos();
		modelo.inicializar();
		VentanaInicial vista =  new VentanaInicial(modelo);
		vista.inicializar();		
	}	
}
