package ppii.ui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import Controladores.ControladorVentanaInicial;
import monitorDeRiesgos.MonitorDeRiesgos;
import observer.Observer;
import riesgoPackage.RiesgoInterface;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.JLabel;


import javax.swing.JButton;

public class VentanaInicial extends JFrame implements Observer{

	protected JPanel contentPane;
	protected JTable tablaRiesgos;	
	protected DefaultTableModel modelRiesgos;
	protected String[] nombreColumnas = {"Riesgo"};
	protected JLabel lblRiesgos;
	protected JButton btnCalcular;
	
	MonitorDeRiesgos myModel;
	ControladorVentanaInicial myController;
	
	public VentanaInicial( MonitorDeRiesgos modelo) {
		myModel = modelo;
		myModel.registrar(this);			
	}
	
	public void inicializar() throws InstantiationException, IllegalAccessException {
		myController = makeController();
	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 290, 377);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(34, 43, 208, 232);
		contentPane.add(panel);
		panel.setLayout(null);
	
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 188, 215);
		panel.add(scrollPane);
		
		tablaRiesgos = new JTable();
		tablaRiesgos.setBounds(10, 11, 188, 215);
		panel.add(tablaRiesgos);
		
		modelRiesgos = new DefaultTableModel(null, nombreColumnas);
		tablaRiesgos = new JTable(modelRiesgos);
		tablaRiesgos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaRiesgos.setAutoCreateColumnsFromModel(false);
	
		DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		
		for (int i = 0; i < tablaRiesgos.getColumnCount(); i++) {
			tablaRiesgos.getColumnModel().getColumn(i).setPreferredWidth(200);
			tablaRiesgos.getColumnModel().getColumn(i).setCellRenderer(tcr);
		}
		scrollPane.setViewportView(tablaRiesgos);
					
		lblRiesgos = new JLabel("Seleccione los Riesgos a calcular");
		lblRiesgos.setBounds(46, 11, 218, 21);
		contentPane.add(lblRiesgos);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.setBounds(58, 299, 162, 28);
		contentPane.add(btnCalcular);
		
		dibujar();		
		this.setVisible(true);
		myController.inicializar();
	}
	
	public void dibujar() {
		this.getModelRiesgos().setRowCount(0);
		for(RiesgoInterface riesgo : myModel.getRiesgos()) {
			Object[] fila = {riesgo.getNombre()};
			this.modelRiesgos.addRow(fila);
		}
	}
	
	public ControladorVentanaInicial makeController() {
		return new ControladorVentanaInicial(this);
	}
	

	public DefaultTableModel getModelRiesgos() {
		return modelRiesgos;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JTable getTablaRiesgos() {
		return tablaRiesgos;
	}

	public JLabel getLblRiesgos() {
		return lblRiesgos;
	}

	public JButton getBtnCalcular() {
		return btnCalcular;
	}

	@Override
	public int update() {
		this.dibujar();
		return 0;
	}
	
	public MonitorDeRiesgos getMyModel() {
		return this.myModel;
	}
	
	public ControladorVentanaInicial getMyController() {
		return this.myController;
	}
}