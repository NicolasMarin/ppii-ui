package ppii.ui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import Controladores.ControladorVentanaCalculoDeRiesgo;
import Controladores.ControladorVentanaInicial;
import monitorDeRiesgos.MonitorDeRiesgos;
import observer.Observer;
import pojos.Zona;
import pojos.ZonaRiesgo;
import riesgoPackage.RiesgoInterface;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Frame;
import java.util.List;

import javax.swing.JButton;

public class VentanaCalculoDeRiesgo extends JFrame implements Observer{

	protected JPanel contentPane;
	protected JTable tablaResultado;	
	protected DefaultTableModel modelResultado;
	protected String[] nombreColumnas;// = {"","San Fernando", "Virreyes", "Moreno", "Los Polvorines", "San Miguel"};
	
	MonitorDeRiesgos myModel;
	ControladorVentanaCalculoDeRiesgo myController;
	private List<RiesgoInterface> setRiesgosSeleccionados;
	
	public VentanaCalculoDeRiesgo( MonitorDeRiesgos modelo) {
		myModel = modelo;
		myModel.registrar(this);	
	}
	
	public void inicializar(){
		myController = makeController();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(true);
		setBounds(100, 100, 800, 600);
		setMinimumSize(new Dimension(800, 600));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(34, 43, 700, 500);
		contentPane.add(panel);
		panel.setLayout(null);
	
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 680, 478);
		panel.add(scrollPane);
		
		tablaResultado = new JTable();
		tablaResultado.setBounds(10, 11, 680, 478);
		panel.add(tablaResultado);
		agregarColumnas();
		modelResultado = new DefaultTableModel(null, nombreColumnas);
		tablaResultado = new JTable(modelResultado);
		tablaResultado.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaResultado.setAutoCreateColumnsFromModel(false);
	
		DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		
		for (int i = 0; i < tablaResultado.getColumnCount(); i++) {
			tablaResultado.getColumnModel().getColumn(i).setPreferredWidth(200);
			tablaResultado.getColumnModel().getColumn(i).setCellRenderer(tcr);
		}
		scrollPane.setViewportView(tablaResultado);
		
		
		dibujar();		
		this.setVisible(true);
		myController.inicializar();
	}
	
	public ControladorVentanaCalculoDeRiesgo makeController() {
		return new ControladorVentanaCalculoDeRiesgo(this);
	}
	
	public void agregarColumnas() {
		int tamanio = this.myModel.getZonas().size();
		nombreColumnas = new String[tamanio+1];
		nombreColumnas[0]="";
		int i=1;
		for(Zona zona: this.myModel.getZonas()) {
			nombreColumnas[i]=zona.getNombre();
			i++;
		}
	}
	
	public void dibujar() {
		this.getModelResultado().setRowCount(0);

		for(RiesgoInterface riesgo : this.setRiesgosSeleccionados) {
			Object[] fila = {riesgo.getNombre()};
			myModel.calcularRiesgoParaTodasLasZonas(riesgo);
			this.modelResultado.addRow(fila);
		}
		
		
		for(int i=0; i< modelResultado.getRowCount(); i++) {
			String riesgo =(String) modelResultado.getValueAt(i, 0);
			for(int j=1; j< modelResultado.getColumnCount(); j++) {
				String zona= modelResultado.getColumnName(j);
				for(ZonaRiesgo zonaRiesgo : myModel.getZonasRiesgos()) {					
					if(riesgo.equals(zonaRiesgo.getNombreRiesgo()) && zona.equals(zonaRiesgo.getNombreZona())) {
						modelResultado.setValueAt(zonaRiesgo.getNivelRiesgo(), i, j);
					}
				}
			}
		}
	}

	public DefaultTableModel getModelResultado() {
		return modelResultado;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JTable getTablaRiesgos() {
		return tablaResultado;
	}

	@Override
	public int update() {
		this.dibujar();
		return 0;
	}
	
	public MonitorDeRiesgos getMyModel() {
		return this.myModel;
	}

	public void setRiesgosSeleccionados(List<RiesgoInterface> riesgos) {
		this.setRiesgosSeleccionados= riesgos;
		
	}
}